import 'package:flutter_test/flutter_test.dart';
import 'package:nonstop_service/nonstop_service.dart';

void main() {
  group('test authentication function', () {
    test('should return true if register is success', () async {
      final repository = AuthenticationRepository();

      final result = await repository.register(
        'https://nonstop.demoteknologi.com/api/auth/register',
        type: 'MEMBER',
        fullName: 'TDD',
        email: 'tdd@gmail.com',
        phone: '081808625505',
        provinceId: '1',
        cityId: '1',
        districtId: '1',
        postcode: '1',
        address: 'wayoo',
      );

      expect(result, true);
    });

    test('should return true if login is success', () async {
      final repository = AuthenticationRepository();

      final result = await repository.authenticate(
        'https://nonstop.demoteknologi.com/api/auth/login',
        phone: '081808625505',
      );
      
      expect(result, true);
    });
  });
}

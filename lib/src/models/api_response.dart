import 'package:meta/meta.dart';

class ApiResponse {
  ApiResponse({
    @required this.statusCode,
    @required this.success,
    @required this.message,
  });

  final int statusCode;
  final bool success;
  final String message;

  factory ApiResponse.fromJson(Map<String, dynamic> json) {
    return ApiResponse(
      statusCode: json["status_code"],
      success: json["success"],
      message: json["message"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "status_code": statusCode,
      "success": success,
      "message": message,
    };
  }
}

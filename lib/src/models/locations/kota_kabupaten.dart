import 'package:meta/meta.dart';

class KotaKabupatenResponse {
  KotaKabupatenResponse({
    @required this.kotaKabupaten,
  });

  final List<KotaKabupaten> kotaKabupaten;

  factory KotaKabupatenResponse.fromJson(Map<String, dynamic> json) {
    return KotaKabupatenResponse(
      kotaKabupaten: List<KotaKabupaten>.from(
          json['kota_kabupaten'].map((x) => KotaKabupaten.fromJson(x))),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'kota_kabupaten': List<dynamic>.from(
        kotaKabupaten.map((x) => x.toJson()),
      ),
    };
  }
}

class KotaKabupaten {
  KotaKabupaten({
    @required this.id,
    @required this.idProvinsi,
    @required this.nama,
  });

  final int id;
  final String idProvinsi;
  final String nama;

  factory KotaKabupaten.fromJson(Map<String, dynamic> json) {
    return KotaKabupaten(
      id: json['id'],
      idProvinsi: json['id_provinsi'],
      nama: json['nama'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'id_provinsi': idProvinsi,
      'nama': nama,
    };
  }
}

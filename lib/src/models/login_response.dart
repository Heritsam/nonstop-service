import 'package:meta/meta.dart';

class LoginResponse {
  LoginResponse({
    @required this.apiToken,
    @required this.tokenType,
    @required this.smsVerify,
    @required this.expiresIn,
  });

  final String apiToken;
  final String tokenType;
  final String smsVerify;
  final DateTime expiresIn;

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
      apiToken: json["api_token"],
      tokenType: json["token_type"],
      smsVerify: json["sms_verify"],
      expiresIn: DateTime.parse(json["expires_in"]),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "api_token": apiToken,
      "token_type": tokenType,
      "sms_verify": smsVerify,
      "expires_in": expiresIn.toIso8601String(),
    };
  }
}
